<?php
/*
Plugin Name: Awesomatic Support
Plugin URI: http://www.awesomatic.nl/plugins/support
Description: Making Customer support awesome and automatic
Version: 0.1
Author: Awesomatic
Author URI: http://www.awesomatic.nl
*/

// Our custom post type function @https://www.wpbeginner.com/wp-tutorials/how-to-create-custom-post-types-in-wordpress/

function create_posttype() {

    register_post_type( 'awesomatic-docs', // Give it a unique name to prevent future problems.
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Docs' ), // This one will show up in the menu, so start with a capital letter.
                'singular_name' => __( 'docs' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'awesomatic-docs'), // Give it a unique name to prevent future problems.
        )
    );
}

// Hooking up our function to theme setup
//add_action( 'init', 'create_posttype' );


/*
* Creating a function to create our CPT
*/

function custom_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'docs', 'Post Type General Name', 'jordiradstake' ),
        'singular_name'       => _x( 'docs', 'Post Type Singular Name', 'jordiradstake' ),
        'menu_name'           => __( 'docs', 'jordiradstake' ),
        'parent_item_colon'   => __( 'Parent docs', 'jordiradstake' ),
        'all_items'           => __( 'All docs', 'jordiradstake' ),
        'view_item'           => __( 'View docs', 'jordiradstake' ),
        'add_new_item'        => __( 'Add New docs', 'jordiradstake' ),
        'add_new'             => __( 'Add New', 'jordiradstake' ),
        'edit_item'           => __( 'Edit docs', 'jordiradstake' ),
        'update_item'         => __( 'Update docs', 'jordiradstake' ),
        'search_items'        => __( 'Search docs', 'jordiradstake' ),
        'not_found'           => __( 'Not Found', 'jordiradstake' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'jordiradstake' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'docs', 'jordiradstake' ),
        'description'         => __( 'docs news and reviews', 'jordiradstake' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', ), // 'comments', 'custom-fields',
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    // Registering your Custom Post Type
    //register_post_type( 'awesomatic-docs', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

//add_action( 'init', 'custom_post_type', 0 );
